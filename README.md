# soal-shift-sisop-modul-3-ITA06-2022
Kelompok ITA06

1. Alda Risma Harjian (5027201004)
2. Richard Nicolas (5027201021)
3. Dzaki Indra Cahya (5027201053)

# Daftar Isi
* [Soal 1](#soal-1) 
  * [Penyelesaian.](#penyelesaian-soal-1) 
  * [Output.](#output-soal-1) 
* [Soal 2](#soal-2) 
   * [Penyelesaian.](#penyelesaian-soal-2) 
  * [Output.](#output-soal-2) 
* [Soal 3](#soal-3) 
  * [Penyelesaian.](#penyelesaian-soal-3) 
  * [Output.](#output-soal-3) 

# Soal-1
Novak adalah seorang mahasiswa biasa yang terlalu sering berselancar di internet. Pada suatu saat, Ia menemukan sebuah informasi bahwa ada suatu situs yang tidak memiliki pengguna. Ia mendownload berbagai informasi yang ada dari situs tersebut dan menemukan sebuah file dengan tulisan yang tidak jelas. Setelah diperhatikan lagi, kode tersebut berformat base64. Ia lalu meminta kepada anda untuk membuat program untuk memecahkan kode-kode di dalam file yang Ia simpan di drive dengan cara decoding dengan base 64. Agar lebih cepat, Ia sarankan untuk menggunakan thread.<br><br>
a. Download dua file zip dan unzip file zip tersebut di dua folder yang berbeda dengan nama quote untuk file zip quote.zip dan music untuk file zip music.zip. Unzip ini dilakukan dengan bersamaan menggunakan thread.<br>
b. Decode semua file .txt yang ada dengan base 64 dan masukkan hasilnya dalam satu file .txt yang baru untuk masing-masing folder (Hasilnya nanti ada dua file .txt) pada saat yang sama dengan menggunakan thread dan dengan nama quote.txt dan music.txt. Masing-masing kalimat dipisahkan dengan newline/enter.<br>
c. Pindahkan kedua file .txt yang berisi hasil decoding ke folder yang baru bernama hasil.<br>
d. Folder hasil di-zip menjadi file hasil.zip dengan password 'mihinomenest[Nama user]'. (contoh password : mihinomenestnovak)<br>
e. Karena ada yang kurang, kalian diminta untuk unzip file hasil.zip dan buat file no.txt dengan tulisan 'No' pada saat yang bersamaan, lalu zip kedua file hasil dan file no.txt menjadi hasil.zip, dengan password yang sama seperti sebelumnya.<br>
<li>Catatan :</li>
<ul><li>Boleh menggunakan execv(), tapi bukan mkdir, atau system</li>
<li>Jika ada pembenaran soal, akan di-update di catatan</li>
<li>Kedua file .zip berada di folder modul</li>
<li>Nama user di passwordnya adalah nama salah satu anggota kelompok, baik yang mengerjakan soal tersebut atau tidak. Misalkan satu kelompok memiliki anggota namanya Yudhistira, Werkudara, dan Janaka. Yang mengerjakan adalah Janaka. Nama passwordnya bisa mihinomenestyudhistira, mihinomenestwerkudara, atau mihinomenestjanaka</li>
<li>Menggunakan apa yang sudah dipelajari di Modul 3 dan, kalau  perlu, di Modul 1 dan 2</li></ul>

# Penyelesaian-soal-1
```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <pthread.h>
#include <unistd.h>
#include <wait.h>
#include <dirent.h>

char make_dir[] = "/bin/mkdir";
char unzipping[] = "/usr/bin/unzip";
char zipping[] = "/usr/bin/zip";
char moving[] = "/usr/bin/mv";

//fungsi untuk memproses sebuah perintah
void processing(char *str, char *argv[])
{
  pid_t child_id;
  child_id = fork();
  int status;

  if (child_id == 0)
  {
    execv(str, argv);
  }
  else
  {
    while (wait(&status) > 0);
  }
}
```
Pertama-tama, kami mendefinisikan modul header yang digunakan dan beberapa variabel global dan fungsi `processing` (menggunakan fork dan execv) untuk memudahkan dalam mengeksekusi perintah seperti unzip, make directory, zip, dan move file 
## Penyelesaian 1a Unzip file `music.zip` dan `quote.zip`
```c
void *unzip(void *arg){
    int *n = (int *)arg;
    int num = *n;
    char *music[] = {"unzip", "-qo", "/home/richard/Downloads/music.zip", "-d", "/home/richard/soal-shift-sisop-modul-3-ita06-2022/soal1/music", NULL};
    char *quote[] = {"unzip", "-qo", "/home/richard/Downloads/quote.zip", "-d", "/home/richard/soal-shift-sisop-modul-3-ita06-2022/soal1/quote", NULL};

    if (num == 0)
    {
        processing(unzipping,music);
    }
    else{
        processing(unzipping,quote);
    }
}
```
Fungsi unzip diatas berfungsi untuk melaukan unzip file `music.zip` dan `quote.zip` yang berada di `Downloads` dan hasilnya dimasukkan ke dalam folder music dan quote. Fungsi ini akan dijalankan menggunakan thread pada bagian main.<br>
Fungsi tersebut akan menerima sebuah nilai, setelah itu dikonversikan menjadi integer dan dimasukkan ke variabel num dan selanjutnya dicek jika nilainya 0, akan melakukan unzip music. Selain 0 akan dilakukan unzip quote.
```c
int main(){
    char* music[]={"mkdir","music",NULL};
    char* quote[]={"mkdir","quote",NULL};
    processing(make_dir,music);
    processing(make_dir,quote);
    int m[]={0,1};
    pthread_t t_id[2];
    for (int i = 0; i < 2; i++)
    {
        pthread_create(&(t_id[i]), NULL, &unzip, (void *)&m[i]);
    }
    for (int i = 0; i < 2; i++)
    {
        pthread_join(t_id[i], NULL);
    }
    ...
}
```
Untuk menyelesaikan soal no 1a, pertama akan dibuat folder music dan quote menggunakan fungsi processing, lalu didefinisikan array variabel thread bernama `t_id` yang memiliki tipe data`pthread_t`. Setelah itu, dilakukan perulangan untuk melakukan pemanggilan fungsi `unzip` dan dipassingkan sebuah nilai yaitu `m`, caranya menggunakan `pthread_create(&(t_id[i]), NULL, &unzip, (void *)&m[i]);`. Setelah menjalankan tread fungsi unzip tersebut maka akan dilakukan join agar thread tersebut dapat dijalankan bersamaan, menggunakan `pthread_join(t_id[i], NULL);`

## Penyelesaian 1b Decode semua file yang ada di folder music dan quote, hasilnya dimasukkan di `music.txt` dan `quote.txt` (revisi)
```c
//decoding base64
static char encoding_table[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H','I', 'J', 'K', 'L', 
                                'M', 'N', 'O', 'P','Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
                                'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f','g', 'h', 'i', 'j', 
                                'k', 'l', 'm', 'n','o', 'p', 'q', 'r', 's', 't', 'u', 'v',
                                'w', 'x', 'y', 'z', '0', '1', '2', '3','4', '5', '6', '7', 
                                '8', '9', '+', '/'};
static char *decoding_table = NULL;
static int mod_table[] = {0, 2, 1};

void build_decoding_table()
{

    decoding_table = malloc(256);

    for (int i = 0; i < 64; i++)
        decoding_table[(unsigned char)encoding_table[i]] = i;
}

void base64_cleanup()
{
    free(decoding_table);
}

char *base64_encode(const unsigned char *data,size_t input_length,size_t *output_length)
{

    *output_length = 4 * ((input_length + 2) / 3);

    char *encoded_data = malloc(*output_length);
    if (encoded_data == NULL)
        return NULL;

    for (int i = 0, j = 0; i < input_length;)
    {

        uint32_t octet_a = i < input_length ? (unsigned char)data[i++] : 0;
        uint32_t octet_b = i < input_length ? (unsigned char)data[i++] : 0;
        uint32_t octet_c = i < input_length ? (unsigned char)data[i++] : 0;

        uint32_t triple = (octet_a << 0x10) + (octet_b << 0x08) + octet_c;

        encoded_data[j++] = encoding_table[(triple >> 3 * 6) & 0x3F];
        encoded_data[j++] = encoding_table[(triple >> 2 * 6) & 0x3F];
        encoded_data[j++] = encoding_table[(triple >> 1 * 6) & 0x3F];
        encoded_data[j++] = encoding_table[(triple >> 0 * 6) & 0x3F];
    }

    for (int i = 0; i < mod_table[input_length % 3]; i++)
        encoded_data[*output_length - 1 - i] = '=';

    return encoded_data;
}

unsigned char *base64_decode(const char *data,size_t input_length,size_t *output_length)
{

    if (decoding_table == NULL)
        build_decoding_table();

    if (input_length % 4 != 0)
        return NULL;

    *output_length = input_length / 4 * 3;
    if (data[input_length - 1] == '=')
        (*output_length)--;
    if (data[input_length - 2] == '=')
        (*output_length)--;

    unsigned char *decoded_data = malloc(*output_length);
    if (decoded_data == NULL)
        return NULL;

    for (int i = 0, j = 0; i < input_length;)
    {

        uint32_t sextet_a = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        uint32_t sextet_b = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        uint32_t sextet_c = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        uint32_t sextet_d = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];

        uint32_t triple = (sextet_a << 3 * 6) + (sextet_b << 2 * 6) + (sextet_c << 1 * 6) + (sextet_d << 0 * 6);

        if (j < *output_length)
            decoded_data[j++] = (triple >> 2 * 8) & 0xFF;
        if (j < *output_length)
            decoded_data[j++] = (triple >> 1 * 8) & 0xFF;
        if (j < *output_length)
            decoded_data[j++] = (triple >> 0 * 8) & 0xFF;
    }

    return decoded_data;
}
```
Beberapa fungsi diatas digunakan untuk melaukan encoding dan decoding base64 secara umum dan biasa ditemukan
```c
void *decode(void *arg)
{
    int *n = (int *)arg;
    int num = *n;

    if (num == 0)
    {
        FILE *mout = fopen("music.txt", "w");
        for (int i = 1; i < 10; i++)
        {
            char c[100];
            FILE *fptr;
            char path[20] = "./music/m";
            char temp[20];
            sprintf(temp, "%d.txt", i);
            strcat(path, temp);
            fptr = fopen(path, "r");
            fscanf(fptr, "%[^\n]", c);
            long decode_size = strlen(c);
            char *plain = base64_decode(c, decode_size, &decode_size);
            fprintf(mout, "%s\n", plain);
            fclose(fptr);
        }
        fclose(mout);
    }
    else
    {
        FILE *qout = fopen("quote.txt", "w");
        for (int i = 1; i < 10; i++)
        {
            char c[100];
            FILE *fptr;
            char path[20] = "./quote/q";
            char temp[20];
            sprintf(temp, "%d.txt", i);
            strcat(path, temp);
            fptr = fopen(path, "r");
            fscanf(fptr, "%[^\n]", c);
            long decode_size = strlen(c);
            char *plain = base64_decode(c, decode_size, &decode_size);
            fprintf(qout, "%s\n", plain);
            fclose(fptr);
        }
        fclose(qout);
    }
}
```
Fungsi `decode` diatas digunakan untuk men-decode setiap file yang ada di folder `music` dan `quote` dan dijalankan menggunakan thread seperti pada 1a pada saat unzip file. Fungsi tersebut akan dijalankan berdasarkan num, jika num 0, maka akan dilakukan decode file pada folder music, selain itu akan dilakukan decode file pada folder quote.<br>
Untuk setiap num akan dibuat file text dan akan dibuka terlebih dahulu menggunakan `fopen("music.txt","w")` atau `fopen("quote.txt","w")` yang fungsinya melakukan `write` pada file text tersebut. Setelah itu akan dilakukan perulangan for dengan i 0-9 untuk membaca setiap file di folder tersebut (setiap folder memiliki 10 file). Untuk mendapatkan nama file digunakan variabel `path` yang merupakan awalan setiap file yaitu `./music/m` lalu menggunakan variabel `temp` yang merupakan gabungan dari `i` dan `.txt`, setelah itu digabungkan variabel `path` dan `temp` dan didapat setiap nama file dan path (contoh : `./music/m0.txt`) <br>
Setelah mendapatkan path dari setiap file, akan dilakukan open file menggunakan `fopen(path, "r")` untuk membaca isi file dan dilakukan decoding menggunakan fungsi `base64_decode()` dan hasilnya dituliskan pada file text yang dibuka dan bisa di-`write` dan menutup file text read yang dibuka tadi menggunakan `fclose()`, setelah semua file text read di-decode dan dimasukkan ke file text write, file text write akan ditutup menggunakan `fclose()`
```c
int main(){
    ...
    int n[]={0,1};
    pthread_t t_idd[2];
    for (int i = 0; i < 2; i++)
    {
        pthread_create(&(t_idd[i]), NULL, &decode, (void *)&n[i]);
    }
    for (int i = 0; i < 2; i++)
    {
        pthread_join(t_idd[i], NULL);
    }
    ...
}
```
Kode diatas terdapat di bagian main yang digunakan untuk melakukan pemanggilan fungsi `decode` menggunakan thread seperti pada bagian sebelumnya pada 1a
## Penyelesaian 1c Pindahkan file hasil decoding ke folder `hasil` (revisi)
```c
void move_file()
{
    char* file_music[] = {"mv", "music.txt", "./hasil", NULL};
    char* file_quote[] = {"mv", "quote.txt", "./hasil", NULL};
    processing(moving,file_music);
    processing(moving,file_quote);
}
```
Fungsi `move_file()` diatas berfungsi untuk memindahkan file `music.txt` dan `quote.txt` ke dalam folder `hasil`, caranya dengan didefinisika variabel yang berisi array dari `"mv"`,`nama file.txt`,`folder tujuan (./hasil)`, dan `NULL`. Setelah itu menggunakan variabel global `moving` dan fungsi `processing` untuk menjalankan perintah move file.
```c
int main(){
    ...
    char* hasil[]={"mkdir","hasil",NULL};
    processing(make_dir,hasil);
    move_file();
    ...
}
```
Program diatas berada pada main yang dimulai dengan pembuatan folder `hasil` variabel global `make_dir` dan fungsi `processing` untuk menjalankan perintah make directory. 
## Penyelesaian 1d Zip folder hasil dengan password `mihinomenestrichard` (revisi)
```c
void zip()
{
    char *zip_file[] = {"zip", "-r", "-m", "-q", "hasil.zip", "-P", "mihinomenestrichard", "hasil", NULL};
    processing(zipping,zip_file);
}
```
Fungsi `zip()` diatas berfugsi untuk melakukan zip folder `hasil` menjadi `hasil.zip` dengan password `mihinomenestrichard` untuk melakukan unzipping. Dilakukan dengan variabel global `zipping` dan fungsi `processing` untuk menjalankan perintah zip folder.<br>
Pada bagian main dipanggil menggunkan `zip();` 
## Penyelesaian 1e Menambahkan sebuah file `no.txt` ke folder hasil dan zip folder tersebut dengan password `mihinomenestrichard` (revisi)
```c
void add_file_text()
{
    char *unzip_hasil[] = {"unzip", "-P", "mihinomenestrichard", "-qo", "hasil.zip", "-d", "/home/richard/soal-shift-sisop-modul-3-ita06-2022/soal1", NULL};
    processing(unzipping,unzip_hasil);
    FILE *add_file = fopen("no.txt", "w");
    fprintf(add_file, "No");
    fclose(add_file);
    char* file_no[] = {"mv", "no.txt", "./hasil", NULL};
    processing(moving,file_no);
}
```
Fungsi diatas merupakan fungsi `add_file_text()` untuk melakukan unzip `hasil.zip` yang telah ada dan dimasukkan sebuah file text bernama `no.text` yang isinya `No`. Pertama-tama dilakukan unzipping dengan password `mihinomenestrichard` dan pemanggilan fungsi `processing` untuk melakukan perintah unzip. Setelah itu dilakukan pembuatan file text baru degan variabel `add_file` yang isinya `fopen("no.txt", "w")` yang isinya dimasukkan `"No"` dengan `fprintf(add_file, "No");` dan setelah itu dilakukan `fclose(add_file);` untuk menutup file text, maka file `no.txt` telah terbentuk. File text kemudian akan dipindahkan ke folder `hasil` dengan fungsi `processing`.
<br>Pada main akan dipanggil fungsi tersebut dan kemudian dilakukan zip seperti pada bagian 1d
# Output-Soal-1
## Output 1a Unzip file `music.zip` dan `quote.zip` 
![1a](img/output 1a0.png) <br>
![1a](img/output 1a1.png) <br><br>
## Output 1b Decode semua file yang ada di folder music dan quote, hasilnya dimasukkan di `music.txt` dan `quote.txt` (revisi)
![1b](img/output 1b.png) <br><br>
## Output 1c Pindahkan file hasil decoding ke folder `hasil` (revisi)
![1c](img/output 1c.png) <br><br>
## Output 1d Zip folder hasil dengan password `mihinomenestrichard` (revisi)
![1d](img/output 1d0.png) <br>
![1d](img/output 1d1.png) <br>
![1d](img/output 1d2.png) <br>
![1d](img/output 1d3.png) <br><br>
## Output 1e Menambahkan sebuah file `no.txt` ke folder hasil dan zip folder tersebut dengan password `mihinomenestrichard` (revisi)
![1e](img/output 1d0.png) <br>
![1e](img/output 1e0.png) <br>
![1e](img/output 1e1.png) <br><br>

# Soal 2
Bluemary adalah seorang Top Global 1 di salah satu platform online judge. Suatu hari Ia ingin membuat online judge nya sendiri, namun dikarenakan Ia sibuk untuk mempertahankan top global nya, maka Ia meminta kamu untuk membantunya dalam membuat online judge sederhana. Online judge sederhana akan dibuat dengan sistem client-server dengan beberapa kriteria sebagai berikut:   <br>




# Penyelesaian-soal-2
# A.Register dan Login
Ketika Client dijalankan, Client akan mencoba untuk melakukan koneksi dengan server menggunakan socket. Terdapat beberapa kasus yang akan menyebabkan kegagalan dalam pengubungan server dan client yang akan mengembalikan pesan error seperti kegagalan dalam pembuatan socket yang akan mengembalikan "Failed to create socket", kegagalan dalam pencarian alamat address yang akan mengembalikan "Invalid address", dan kegagalan penyambungan koneksi yang akan mengembalikan "Connection failed". Jika koneksi berhasil dibuat, akan muncul pesan agar client mengetikkan sesuatu supaya server bisa mengkonfirmasi bahwa koneksi telah terjalin. <br>
```
struct sockaddr_in alamatServer;
  char *login = "login", *logout = "logout", *regist = "register";
  char temp, buffer[1100] = {0}, cmd[1100], username[1100], password[1100];
  if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
  {
    printf(Failed to create socket);
    return -1;
  }
  memset(&alamatServer, '0', sizeof(alamatServer));
  alamatServer.sin_family = AF_INET, alamatServer.sin_port = htons(port);
  if (inet_pton(AF_INET, "127.0.0.1", &alamatServer.sin_addr) <= 0)
  {
    printf("Invalid address\n");
    return -1;
  }
  if (connect(sock, (struct sockaddr *)&alamatServer, sizeof(alamatServer)) < 0)
  {
    printf("Connection failed\n");
    return -1;
  }
  readerConn = read(sock, buffer, 1100);
  printf("%s\n", buffer);
  while (!same("Connected", buffer))
  {
    printf("Enter anything to check whether you can connect to the server or not.\n");
    scanf("%s", cmd);
    send(sock, cmd, 1100, 0);
    readerConn = read(sock, buffer, 1100);
    printf("%s\n", buffer);
  }
  if (!same("Connected", buffer))
    return 0;

```
etelah terjalin koneksi antara Client dan Server, akan muncul pilihan untuk Client melakukan login, register, atau exit. Fungsi scanf akan mengambil command dari client dan menjalankan fungsi selanjutnya sesuai dengan command yang diberikan oleh client.<br>
```
 while (1)
  {
    printf("1. Login\n2. Register\n3. Exit\n");
    scanf("%s", cmd);
```
Jika client memasukkan command login, maka akan muncul pesan agar client memasukkan username dan password yang akan dikirimkan kepada server. Jika dikembalikan "LoginSuccess" dari server, client akan dapat masuk ke tahap berikutnya dan pesan bahwa login berhasil akan ditampilkan. Namun, jika tidak dikembalikan "LoginSuccess" dari server, akan ditampilkan pesan bahwa proses login telah gagal dan client dapat mencoba untuk memasukkan kembali informasinya. <br>
```
 while (1)
  {
    if (same(cmd, login))
    {
      send(sock, login, strlen(login), 0);
      printf("Username: ");
      scanf("%c", &temp);
      scanf("%[^\n]", username);
      send(sock, username, 1100, 0);
      printf("Password: ");
      scanf("%c", &temp);
      scanf("%[^\n]", password);
      send(sock, password, 1100, 0);
      memset(buffer, 0, sizeof(buffer));
      readerConn = read(sock, buffer, 1100);
      if (same(buffer, "LoginSuccess"))
        printf("Login process is success! Welcome aboard! ^o^\n");
      else
        printf("Login process failed, please try again.\n");
```
Di sisi server, server akan membaca informasi login yang dikirimkan oleh klien dan membuka file user.txt dengan fopen mode "r" untuk membaca data user yang tersedia. Server akan membaca file pada user.txt dan melakukan compare terhadap informasi login yang telah diberikan oleh user untuk menentukan apakah user berhasil login atau tidak. Jika ditemukan informasi login yang valid, nilai check akan menjadi 1 dan server akan mengirimkan "LoginSuccess" sehingga client akan dapat melanjutkan ke tahap berikutnya. <br>
```
while (1)
  {
    readerConn = read(serverSocket, buffer, 1100);
    if (same(login, buffer))
    {
      readerConn = read(serverSocket, user.name, 1100);
      readerConn = read(serverSocket, user.password, 1100);
      fp3 = fopen("users.txt", "r");
      int check = 0;
      char *line = NULL;
      ssize_t len = 0;
      ssize_t fileReader;
      while ((fileReader = getline(&line, &len, fp3) != -1))
      {
        char userName[105], userPass[105];
        int i = 0, j = 0;
        while (line[i] != ':')
          userName[i] = line[i], i++;
        userName[i++] = '\0';
        while (line[i] != '\n')
          userPass[j] = line[i], j++, i++;
        userPass[j] = '\0';
        if (strcmp(user.name, userName) == 0 && strcmp(user.password, userPass) == 0)
        {
          check = 1, send(serverSocket, "LoginSuccess", strlen("LoginSuccess"), 0);
          break;
        }
      }
```
Namun, jika informasi login tidak sesuai dengan data yang dimiliki server, nilai check akan tetap 0 dan server akan mengirimkan "LoginFail" kepada client sehingga client harus memasukkan kembali informasi login. <br>
```
  if (check == 0)
        printf("Authorization for Login is Failed\n"), send(serverSocket, "LoginFail", strlen("LoginFail"), 0);
      else
      {
        printf("Authorization for Login is Success\n");
```
Di sisi server, server akan membuka file user.txt dengan mode "a" dan "r" untuk melakukan append dan membaca file user.txt. Server akan menerima informasi registrasi kemudian mengecek dengan melakukan compare antara username yang ada di file.txt dan username yang diterima dari client. Jika tidak ditemukan perbedaan, maka username telah dipakai dan server akan melakukan break pada operasi dan mengembalikan "RegError". Jika pengecekan username berhasil dilewati tanpa break, server akan mengecek adanya minimal karakter, huruf kapital, huruf kecil, dan angka pada password dengan menggunakan nilai ASCII. Jika terdapat ketiganya, maka server akan melakukan append username dan password yang dimasukkan dalam file.txt dan mengembalikan "Register Berhasil" kepada client. <br>
```
 else if (same(regist, buffer))
    {
      fp2 = fopen("users.txt", "a"), fp3 = fopen("users.txt", "r");
      readerConn = read(serverSocket, user.name, 1100),
      readerConn = read(serverSocket, user.password, 1100);
      int flag = 0;
      char *line = NULL;
      ssize_t len = 0;
      ssize_t fileReader;
      int check = 0, numUsers = 0;
      while ((fileReader = getline(&line, &len, fp3) != -1))
      {
        numUsers++;
        char userName[105], userPass[105];
        int i = 0;
        while (line[i] != ':')
          userName[i] = line[i], i++;
        userName[i] = '\0';
        i++;
        int j = 0;
        while (line[i] != '\n')
          userPass[j] = line[i], j++, i++;
        userPass[j] = '\0';
        if (strcmp(user.name, userName) == 0)
        {
          check = 0;
          break;
        }
        int kapital = 0, lower = 0, angka = 0, jumlah = 0;
        for (int i = 0; i < strlen(user.password); i++)
        {
          if (user.password[i] >= 48 && user.password[i] <= 57)
            angka = 1;
          if (user.password[i] >= 65 && user.password[i] <= 90)
            kapital = 1, jumlah++;
          if (user.password[i] >= 97 && user.password[i] <= 122)
            lower = 1, jumlah++;
        }
        if (!angka || !lower || !kapital || jumlah < 6)
          break;
        if (angka && lower && kapital && jumlah >= 6)
          check = 1;
      }
      if (check || numUsers == 0)
      {
        fprintf(fp2, "%s:%s\n", user.name, user.password);
        send(serverSocket, "Register berhasil", strlen("Register berhasil"), 0);
      }
      else
        send(serverSocket, "RegError", strlen("RegError"), 0);
      fclose(fp2);
    }
    else if (same("exit", buffer) || same("3", buffer))
    {
      close(serverSocket), total--;
      break;
    }
  }
```
# B. Database Problem
Jika server berhasil dijalankan, akan dieksekusi fungsi fopen mode "a" yang akan membuat file problems.tsv jika sebelumnya file tersebut belum ada. <br>
```
int main(int argc, char const *argv[])
{
  struct sockaddr_in alamat;
  int serverSocket, serverFD, readerConn, opt = 1, jumlahKoneksi = 5, porter = 8080, addr_len = sizeof(alamat);
  if ((serverFD = socket(AF_INET, SOCK_STREAM, 0)) == 0)
    perror("socket failed"), exit(EXIT_FAILURE);
  if (setsockopt(serverFD, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt)))
    perror("setsockopt"), exit(EXIT_FAILURE);
  alamat.sin_family = AF_INET, alamat.sin_addr.s_addr = INADDR_ANY, alamat.sin_port = htons(porter);
  if (bind(serverFD, (struct sockaddr *)&alamat, sizeof(alamat)) < 0)
    perror("bind failed"), exit(EXIT_FAILURE);
  if (listen(serverFD, jumlahKoneksi) < 0)
    perror("listen"), exit(EXIT_FAILURE);
  mkdir("problems", 0777);
  FILE *fp = fopen("problems.tsv", "a");
```
User dapat menambahkan problem pada file ini dengan command add yang akan dijelaskan pada bagian C. File ini berisi judul problem dan username pembuat problem yang dipisahkan oleh \t sesuai perintah soal. <br>
```
...
            fp = fopen("problems.tsv", "a");
            fprintf(fp, "%s\t%s\n", request.problem_title, user.name), fclose(fp);
...
```

# C. Add Problem
Setelah client berhasil melakukan login, client akan mendapatkan beberapa pilihan. Salah satunya adalah untuk melakukan add problem. <br>
```
 if (same(buffer, "LoginSuccess"))
      {
        while (1)
        {
          printf("1. Logout\n2. Add\n3. Download [Problem Name]\n4. Submit [Problem Name] [Path Output]\n5. See\n");
          scanf("%s", cmd);
          if (same(cmd, logout))
          {
            send(sock, logout, 1100, 0);
            break;
          }
```
Jika client memasukkan command add, akan muncul pesan untuk memasukkan judul problem yang akan diambil menggunakan fungsi scanf dan dikirimkan kepada server. Akan muncul juga pesan untuk memasukkan filepath description.txt yang berisi deskripsi problem, filepath input.txt yang berisi input testcase untuk menyelesaikan problem, dan output.txt yang akan digunakan untuk melakukan pengecekan pada submission client. <br>
```
 else if (same(cmd, "add"))
          {
            send(sock, cmd, 1100, 0);
            char data[1100], problemName[1100];
            printf("Judul Problem: ");
            scanf("\n%[^\n]%*c", problemName);
            send(sock, problemName, 1100, 0);
            printf("Filepath description.txt: ");
            scanf("\n%[^\n]%*c", data);
            char outputFolder[1100], inputFolder[1100], descFolder[1100], outputPath[1100];
            strcpy(descFolder, data);
            removeStr(descFolder, "/description.txt");
            mkdir(descFolder, 0777);
            send(sock, data, 1100, 0);
            printf("Filepath input.txt: ");
            scanf("\n%[^\n]%*c", data);
            strcpy(inputFolder, data);
            removeStr(inputFolder, "/input.txt");
            mkdir(inputFolder, 0777);
            send(sock, data, 1100, 0);
            printf("Filepath output.txt: ");
            scanf("\n%[^\n]%*c", data);
            send(sock, data, 1100, 0);
            readerConn = read(sock, buffer, 1100);
            int numGacha = atoi(buffer);
            memset(buffer, 0, sizeof(buffer));
            strcpy(outputPath, data);
            FILE *fileManager;
            fileManager = fopen(outputPath, "a+");
            for (int i = 0; i < numGacha; i++)
              readerConn = read(sock, buffer, 1100),
              fprintf(fileManager, "%s", buffer),
              memset(buffer, 0, sizeof(buffer));
            fclose(fileManager);
            printf("Successfully adding problem %s to the server!\n", problemName);
            continue;
          }
```
Di sisi server, server akan membaca kiriman informasi dari client dan mengirimakn request untuk judul problem, path description, path input, dan path output. Server kemudian membuat direktori yang mengambil nama dari input judul problem yang dimasukkan oleh client dan menyimpan problem di dalamnya dengan strcpy dan strcat. Setelah itu, akan ditampilkan pesan "(username) is adding new problem called (judul) to the server". <br>
```
else if (same("add", buffer))
          {
            problem request;
            char basePath[1100], clientPath[1100], descPath[1100], inputPath[1100];
            char descServer[1100], inputServer[1100], outputServer[1100];
            readerConn = read(serverSocket, request.problem_title, 1100);
            printf("Judul problem: %s\n", request.problem_title);
            readerConn = read(serverSocket, request.pathDesc, 1100);
            printf("Filepath description.txt: %s\n", request.pathDesc);
            readerConn = read(serverSocket, request.pathInput, 1100);
            printf("Filepath input.txt: %s\n", request.pathInput);
            readerConn = read(serverSocket, request.pathOutput, 1100);
            printf("Filepath output.txt: %s\n", request.pathOutput);
            strcpy(basePath, "problems/");
            strcat(basePath, request.problem_title);
            mkdir(basePath, 0777);
            strcat(basePath, "/");
            strcpy(descServer, basePath);
            strcat(descServer, "description.txt");
            strcpy(inputServer, basePath);
            strcat(inputServer, "input.txt");
            strcpy(outputServer, basePath);
            strcat(outputServer, "output.txt");
            fp = fopen("problems.tsv", "a");
            fprintf(fp, "%s\t%s\n", request.problem_title, user.name), fclose(fp);
            printf("%s is adding new problem called %s to the server.\n", user.name, request.problem_title);
            continue;
          }
```
# D. See Problem
Setelah client berhasil melakukan login, client akan mendapatkan beberapa pilihan. Salah satunya adalah untuk melakukan see problem. <br>
```
 if (same(buffer, "LoginSuccess"))
      {
        while (1)
        {
          printf("1. Logout\n2. Add\n3. Download [Problem Name]\n4. Submit [Problem Name] [Path Output]\n5. See\n");
          scanf("%s", cmd);
          if (same(cmd, logout))
          {
            send(sock, logout, 1100, 0);
            break;
          }
```
Jika user memasukkan command see, akan ditampilkan pesan berupa "Problems Available in Online Judge" dan client akan mengirimkan perintah "see" kepada server. Perintah ini akan membuat server mengembalikan daftar judul dan pembuat problem. <br>
```
else if (same(cmd, "see"))
          {
            printf("Problems Available in Online Judge:\n");
            send(sock, cmd, 1100, 0);
            memset(buffer, 0, sizeof(buffer));
            while (readerConn = read(sock, buffer, 1100))
            {
              if (same(buffer, "e"))
                break;
              printf("%s", buffer);
            }
            continue;
```
Di sisi server, jika server mendapatkan command "see", server akan membuka file problems.tsv dengan fungsi fopen mode "r" untuk membaca problem dan mengirimkan line demi line berisi judul problem dan pengarangnya kepada client sebelum menutup file tersebut dengan fclose. <br>
```
else if (same("see", buffer))
          {
            fp = fopen("problems.tsv", "r");
            if (!fp)
            {
              send(serverSocket, "e", sizeof("e"), 0);
              memset(buffer, 0, sizeof(buffer));
              continue;
            }
            char *line = NULL;
            ssize_t len = 0;
            ssize_t fileReader;
            while ((fileReader = getline(&line, &len, fp) != -1))
            {
              problem temp_problem;
              storeProblemTSV(&temp_problem, line);
              char message[1100];
              sprintf(message, "%s by %s", temp_problem.problem_title, temp_problem.author);
              send(serverSocket, message, 1100, 0);
            }
            send(serverSocket, "e", sizeof("e"), 0), fclose(fp);
            memset(buffer, 0, sizeof(buffer));
          }
        }
```
# E. Download Problem
Setelah client berhasil melakukan login, client akan mendapatkan beberapa pilihan. Salah satunya adalah untuk melakukan download problem.<br>

```
if (same(buffer, "LoginSuccess"))
      {
        while (1)
        {
          printf("1. Logout\n2. Add\n3. Download [Problem Name]\n4. Submit [Problem Name] [Path Output]\n5. See\n");
          scanf("%s", cmd);
          if (same(cmd, logout))
          {
            send(sock, logout, 1100, 0);
            break;
          }
```
Jika client memasukkan command download [namafile], client akan mengirimkan command tersebut menggunakan fungsi send kepada server. Client akan membuka... <br>
```
  else if (same(cmd, "download"))
          {
            send(sock, cmd, 1100, 0);
            memset(buffer, 0, sizeof(buffer));
            scanf("%s", cmd);
            send(sock, cmd, 1100, 0);
            readerConn = read(sock, buffer, 1100);
            int numLine = atoi(buffer);
            char inputTarget[1100];
            readerConn = read(sock, inputTarget, 1100);
            for (int i = 0; i < numLine; i++)
            {
              FILE *fpClient = fopen(inputTarget, "r");
              fpClient = fopen(inputTarget, "a+");
              char fileContent[1100];
              readerConn = read(sock, fileContent, 1100);
              fprintf(fpClient, "%s\n", fileContent), fclose(fpClient);
            }
            memset(buffer, 0, sizeof(buffer));
            readerConn = read(sock, buffer, 1100);
            char fileReady[] = "File is ready to be downloaded.\n", descTarget[1100];
            if (same(buffer, fileReady))
            {
              memset(buffer, 0, sizeof(buffer));
              readerConn = read(sock, buffer, 1100);
              strcpy(descTarget, buffer);
              printf("Downloading description & input from problem %s..\n", cmd);
              int des_fd = open(descTarget, O_WRONLY | O_CREAT | O_EXCL, 0700);
              if (!des_fd)
                perror("can't open file"), exit(EXIT_FAILURE);
              int file_read_len;
              char buff[1100];
              while (1)
              {
                memset(buff, 0, 1100);
                file_read_len = read(sock, buff, 1100);
                write(des_fd, buff, file_read_len);
                break;
              }
              printf("Successfully downloading the files for problem %s to the desired client directory!\n", cmd);
            }
            memset(buffer, 0, sizeof(buffer));
            continue;
```

dan dari server
```
else if (same("download", buffer))
          {
            readerConn = read(serverSocket, buffer, 1100);
            bool foundDesc = false, foundInput = false;
            char pathDownload[1100] = "problems/", descPath[1100], descTarget[1100];
            strcat(pathDownload, buffer);
            strcpy(descPath, pathDownload);
            strcat(descPath, "/description.txt");
            char inputPath[1100], inputTarget[1100], outputPath[1100], outputTarget[1100];
            strcpy(inputPath, pathDownload);
            strcat(inputPath, "/input.txt");
            strcpy(outputPath, pathDownload);
            strcat(outputPath, "/output.txt");
            char foundSuccess[] = "File is ready to be downloaded.\n", notFound[] = "No such file found.\n";
            fp = fopen(descPath, "r");
            FILE *source, *target;
            source = fopen(descPath, "r");
            char *line = NULL, *line2 = NULL, *line3 = NULL;
            ssize_t len = 0;
            ssize_t fileReader;
            int num = 0, fd, readLength;
            while ((fileReader = getline(&line, &len, source) != -1))
            {
              num++;
              if (num > 3)
              {
                char pathContent[105];
                int i = 0, j = 0;
                while (line[i] != ':')
                  i++;
                line[i] = '\0';
                i += 2;
                while (line[i] != '\n')
                  pathContent[j++] = line[i++];
                pathContent[j] = '\0';
                if (num == 4)
                  strcpy(descTarget, pathContent);
                if (num == 5)
                  strcpy(inputTarget, pathContent);
                if (num == 6)
                  strcpy(outputTarget, pathContent);
              }
            }
            int numLine = 0;
            FILE *fpSource = fopen(inputPath, "r");
            ssize_t len2 = 0;
            ssize_t fileReader2;
            bool checkSubmission = true;
            while ((fileReader2 = getline(&line2, &len2, fpSource) != -1))
              numLine++;
            fclose(fpSource);
            fpSource = fopen(inputPath, "r");
            char buf[1100];
            sprintf(buf, "%d", numLine);
            send(serverSocket, buf, 1100, 0);
            send(serverSocket, inputTarget, 1100, 0);
            ssize_t len3 = 0;
            ssize_t fileReader3;
            while ((fileReader3 = getline(&line3, &len3, fpSource) != -1))
            {
              char outputSource[105];
              int i = 0;
              while (line3[i] != '\n')
                outputSource[i] = line3[i], i++;
              outputSource[i] = '\0';
              send(serverSocket, outputSource, 1100, 0);
            }
            fclose(fpSource);
            if (fp)
              foundDesc = true;
            if (!foundDesc)
              send(serverSocket, notFound, 1100, 0);
            else
            {
              send(serverSocket, foundSuccess, 1100, 0);
              send(serverSocket, descTarget, 1100, 0);
              fd = open(descPath, O_RDONLY);
              if (!fd)
                perror("Fail to open"), exit(EXIT_FAILURE);
              while (1)
              {
                memset(descPath, 0x00, 1100);
                readLength = read(fd, descPath, 1100);
                if (readLength == 0)
                  break;
                else
                  send(serverSocket, descPath, readLength, 0);
              }
              close(fd);
            }
            fclose(fp);
          }
```
# F. Submit Problem
Jika client memasukkan command submit [judul-problem] [path-file-output], client akan mengirim command tersebut kepada server kemudian memasukkan [judul-problem] dan [path-file-output] untuk dikirimkan juga kepada server. Client kemudian akan mencoba untuk membuka file dan membacanya. Jika file tidak tersedia, akan dikembalikan pesan "Output file is not exist!" dan break akan terjadi. Jika file tersedia, program akan masuk ke dalam loop untuk mengambil line demi line dari output submission menggunakan fungsi getline untuk dikirimkan kepada server. Setelah tidak ada line untuk diambil lagi, program akan membaca perintah yang dikembalikan oleh server dan menampilkan hasil dari submission. <br>

```
 else if (same(cmd, "submit"))
          {
            send(sock, cmd, 1100, 0);
            memset(buffer, 0, sizeof(buffer));
            char namaProblem[1100], filePath[1100];
            scanf("%s %s", namaProblem, filePath);
            send(sock, namaProblem, 1100, 0);
            FILE *fpC = fopen(filePath, "r");
            if (!fpC)
            {
              printf("Output file is not exist!\n");
              break;
            }
            char *line = NULL;
            ssize_t len = 0;
            ssize_t fileReader;
            bool checkSubmission = true;
            while ((fileReader = getline(&line, &len, fpC) != -1))
            {
              char outputSubmission[105];
              int i = 0, j = 0;
              while (line[i] != ':')
                i++;
              line[i++] = '\0';
              while (line[i] != '\n')
                outputSubmission[j++] = line[i++];
              outputSubmission[j] = '\0';
              send(sock, outputSubmission, 105, 0);
            }
            memset(buffer, 0, sizeof(buffer));
            readerConn = read(sock, buffer, 1100);
            printf("Your output.txt file for problem %s is submitted!\nResult: %s\n", namaProblem, buffer);
          }
```
dan dari sisi server
```
 else if (same("submit", buffer))
          {
            char problemName[1100], pathSource[1100] = "problems/";
            readerConn = read(serverSocket, problemName, 1100);
            strcat(pathSource, problemName);
            strcat(pathSource, "/output.txt");
            FILE *fpSource = fopen(pathSource, "r");
            char *line = NULL;
            ssize_t len = 0;
            ssize_t fileReader;
            bool checkSubmission = true;
            while (fileReader = getline(&line, &len, fpSource) != -1)
            {
              char outputSource[105], outputSubmission[105];
              int i = 0, j = 0;
              while (line[i] != ':')
                i++;
              line[i++] = '\0';
              while (line[i] != '\n')
                outputSource[j++] = line[i++];
              outputSource[j] = '\0';
              readerConn = read(serverSocket, outputSubmission, 105);
              int source = atoi(outputSource), submit = atoi(outputSubmission);
              printf("[%s] source: %d dan submission: %d\n", submit == source ? "AC" : "WA", source, submit);
              if (submit != source)
                checkSubmission = false;
            }
            printf("%s is %s on problem %s!\n", user.name, checkSubmission ? "accepted (AC)" : "having a wrong answer (WA)", problemName);
            send(serverSocket, checkSubmission ? "AC\nAccepted in Every Testcases\n" : "WA\nWA in the First Testcase\n", 1100, 0);
          }
```
# G. Multiple Connection
Program menangani multiple connection dengan mengecek berapa banyak client yang terkoneksi. Jika terdapat tepat satu client, server akan mengirimkan pesan connected kepada client, sedangkan jika tidak maka server akan mengirimkan pesan failed. Selama terbaca bahwa total client lebih dari satu, server akan mengecek banyak client yang terhubung. Jika setelah itu jumlah client adalah satu, maka server akan mengirimkan pesan connected. Namun, jika tidak, server akan mengirimkan pesan failed. Failed akan menampilkan pada client yang gagal terhubung bahwa ada client yang sedang terkoneksi dan untuk menunggu koneksi diakhiri. <br>
```
char buffer[1100] = {0}, connected[1100] = "Connected", failed[1100] = "Other people are connected, please wait for them ^o^";
  int readerConn, serverSocket = *(int *)tmp;
  if (total == 1)
    send(serverSocket, connected, 1100, 0);
  else
    send(serverSocket, failed, 1100, 0);
  while (total > 1)
  {
    readerConn = read(serverSocket, buffer, 1100);
    if (total == 1)
      send(serverSocket, connected, 1100, 0);
    else
      send(serverSocket, failed, 1100, 0);
  }
```


# Output-Soal-2
### 1.Register dan login
![satu](img/2a.png)

### 2.Data base dan problem
![satu](img/2_.2.png)

### 3.Add problem
![satu](img/2.3.png)

### 4.See problem
![satu](img/see problem.png)

### 5. Download problem
![satu](img/download.png)

# Soal 3
Nami adalah seorang pengoleksi harta karun handal. Karena Nami memiliki waktu luang, Nami pun mencoba merapikan harta karun yang dimilikinya berdasarkan jenis/tipe/kategori/ekstensi harta karunnya. Setelah harta karunnya berhasil dikategorikan, Nami pun mengirimkan harta karun tersebut ke kampung halamannya.

**a.**
Hal pertama yang perlu dilakukan oleh Nami adalah mengextract zip yang diberikan ke dalam folder “/home/[user]/shift3/”. Kemudian working directory program akan berada pada folder “/home/[user]/shift3/hartakarun/”. Karena Nami tidak ingin ada file yang tertinggal, program harus mengkategorikan seluruh file pada working directory secara rekursif

**b.**
Semua file harus berada di dalam folder, jika terdapat file yang tidak memiliki ekstensi, file disimpan dalam folder “Unknown”. Jika file hidden, masuk folder “Hidden”.

**c.**
Agar proses kategori bisa berjalan lebih cepat, setiap 1 file yang dikategorikan dioperasikan oleh 1 thread.

**d.**
Untuk mengirimkan file ke Cocoyasi Village, nami menggunakan program client-server. Saat program client dijalankan, maka folder /home/[user]/shift3/hartakarun/” akan di-zip terlebih dahulu dengan nama “hartakarun.zip” ke working directory dari program client.

**e.**
Client dapat mengirimkan file “hartakarun.zip” ke server dengan mengirimkan command 


## Catatan
- Kategori folder tidak dibuat secara manual, harus melalui program C
- Program ini tidak case sensitive. Contoh: JPG dan jpg adalah sama
- Jika ekstensi lebih dari satu (contoh “.tar.gz”) maka akan masuk ke folder dengan titik terdepan (contoh “tar.gz”)
- Dilarang juga menggunakan fork, exec dan system(), kecuali untuk bagian zip pada soal d
# Penyelesaian-soal-3
## A. Extract Zip
Langkah pertama yaitu mengekstrak file hartakarun.zip yang telah disediakan di soal. Lalu menggunakan fungsi popen() dengan menggunakan argumen pertama yaitu value, dimana variabel value berisi path ke file .zip-nya dan argumen kedua yaitu r untuk membaca filenya. Lalu selanjutnya menggunakan fungsi fread pada variabel chars_read akan memprint proses ekstraksi file di dalam arsip ke terminal. Fungsi yang bertanggung jawab dalam hal ini adalah fungsi extract().

```
void extract(char *value) 
{
    FILE *file;
    char buffer[BUFSIZ+1];
    int chars_read;

    memset(buffer,'\0',sizeof(buffer));

    file = popen(value ,"r");

    if(file != NULL){
        chars_read = fread(buffer, sizeof(char), BUFSIZ, file);
        if(chars_read > 0) printf("%s\n",buffer);
        pclose(file);
    }
}
```

## B. Mengkategorikan File dalam Folder
Lalu selanjutnya untuk mengkategorikan file, program akan memanggil fungsi sortFile() untuk menjalankan tugas tersebut. Berikut tampilan fungsi sortFile():

```
void sortFile(char *from)
{
    struct_path s_path;
    struct dirent *dp;
    DIR *dir;

    flag = 1;
    int index = 0;
    strcpy(s_path.cwd, "/home/aga/shift3/hartakarun");

    dir = opendir(from);

    while((dp = readdir(dir)) != NULL){
        if(strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0){
            char fileName[10000];
            sprintf(fileName, "%s/%s", from, dp->d_name);
            strcpy(s_path.from, fileName);

            pthread_create(&tid[index], NULL, move, (void *)&s_path);
            sleep(1);
            index++;
        }
    }

    if(!flag){
        printf("Maaf kawan, lupakan saja :)\n");
    }else {
        printf("Kamu bisa lanjut, tapi jangan berharap lebih :)\n");
    }
}
```

Di fungsi ini program akan membaca directory hasil ekstrak file .zip dan mengecek satu-persatu filenya. Informasi mengenai directory asal dan nama filenya disimpan ke dalam variabel fileName. Tugas ini dilakukan pada potongan code di bawah ini:

```
    dir = opendir(from);

    while((dp = readdir(dir)) != NULL){
        if(strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0){
            char fileName[10000];
            sprintf(fileName, "%s/%s", from, dp->d_name);
            strcpy(s_path.from, fileName);
```

## C. Mengkategorikan File Menggunakan Thread
Pembuatan thread dilakukan pada fungsi sortFile() yang juga memanggil fungsi move().

```
pthread_create(&tid[index], NULL, move, (void *)&s_path);
```

Pada fungsi move() program akan meamnggil fungsi moveFile() kemudian keluar dari thread.

```
{
    struct_path s_paths = *(struct_path*) s_path;
    moveFile(s_paths.from, s_paths.cwd);
    pthread_exit(0);
}
```

Berikut merupakan tampilan fungsi moveFile():

```
void moveFile(char *p_from, char *p_cwd)
{
    char file_name[300], file_ext[30], temp[300];
    char *buffer;

    strcpy(temp, p_from);
    buffer = strtok(temp, "/");

    while(buffer != NULL){
        if(buffer != NULL) strcpy(file_name, buffer);
        buffer = strtok(NULL, "/");
    }

    strcpy(temp, file_name);
    
    buffer = strtok(temp, ".");
    buffer = strtok(NULL, "");

    if(file_name[0] == '.'){
        strcpy(file_ext, "Hidden");
    }else if(buffer != NULL){
        strcpy(file_ext, buffer);
    }else{
        strcpy(file_ext, "Unknown");
    }

    toLower(file_ext);

    // new dest
    strcat(p_cwd, "/");
    strcat(p_cwd, file_ext);
    strcat(p_cwd, "/");
    strcat(p_cwd, file_name);

    // information
    printf("from %s: \nfile_name: %s\nfile_ext: %s\ndest: %s\n\n", p_from, file_name, file_ext, p_cwd);

    // create dir and move file
    createDir(file_ext);
    rename(p_from, p_cwd);
}
```

Langkah awal pada fungsi ini yaitu nama file ke dalam variabel file_name dengan bantuan variabel buffer yang sebelumnya akan diisi nama file dengan bantuan fungsi strtok untuk mencari delimiter "/".

```
    buffer = strtok(temp, "/");

    while(buffer != NULL){
        if(buffer != NULL) strcpy(file_name, buffer);
        buffer = strtok(NULL, "/");
    }
```

Selanjutnya menentukan eksintensi file dengan bantuan variabel buffer yang akan berisi format file setelah diinisiasi hasil dari strtok dengan delimiter ".". Terdapat pengecualian pada pengkategorian file ini yaitu apabila variabel buffer berisi NULL yang artinya eksintensi file akan dikategorikan hidden (apabila file diawali karakter ".") dan unknown (apabila tidak memenuhi kedua kondisi).

```
    strcpy(temp, file_name);
    
    buffer = strtok(temp, ".");
    buffer = strtok(NULL, "");

    if(file_name[0] == '.'){
        strcpy(file_ext, "Hidden");
    }else if(buffer != NULL){
        strcpy(file_ext, buffer);
    }else{
        strcpy(file_ext, "Unknown");
    }
```

Selanjutnya adalah membuat destinasi dari file dengan menambahkan eksistensi dan nama file ke dalam path directory lama menggunakan strcat().

```
    strcat(p_cwd, "/");
    strcat(p_cwd, file_ext);
    strcat(p_cwd, "/");
    strcat(p_cwd, file_name);
```

Kemudian langkah terakhir adalh menampilkan informasi file dan membuat directory berdasarkan eksistensi filenya.

```
    // information
    printf("from %s: \nfile_name: %s\nfile_ext: %s\ndest: %s\n\n", p_from, file_name, file_ext, p_cwd);

    // create dir and move file
    createDir(file_ext);
    rename(p_from, p_cwd);
```

## D. Client-Server dan Pengezipan Harta Karun
MashaAllah susah mas

## E. Send Harta Karun
MashaAllah susah mas

# Output-Soal-3

#### 1. Extract Zip
##### a. home
![satu](img/1.png)
##### b. terminal
![dua](img/1.1.png)

#### 2. Informasi File
![tiga](img/2.png)

#### 3. Hasil Pengkategorian File Berdasarkan Eksistensi
![tiga](img/3.png)

#### 4. Isi Folder Pengecualian
##### a. hidden
![empat](img/3.1.png)
##### b. unknown
![lima](img/3.2.png)

![HIHI](img/HIHI.jpeg)
