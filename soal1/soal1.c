#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <pthread.h>
#include <unistd.h>
#include <wait.h>
#include <dirent.h>

char make_dir[] = "/bin/mkdir";
char unzipping[] = "/usr/bin/unzip";
char zipping[] = "/usr/bin/zip";
char moving[] = "/usr/bin/mv";

//fungsi untuk memproses sebuah perintah
void processing(char *str, char *argv[])
{
  pid_t child_id;
  child_id = fork();
  int status;

  if (child_id == 0)
  {
    execv(str, argv);
  }
  else
  {
    while (wait(&status) > 0);
  }
}

//fungsi unzip file di downloads
void *unzip(void *arg){
    int *n = (int *)arg;
    int num = *n;
    char *music[] = {"unzip", "-qo", "/home/richard/Downloads/music.zip", "-d", "/home/richard/soal-shift-sisop-modul-3-ita06-2022/soal1/music", NULL};
    char *quote[] = {"unzip", "-qo", "/home/richard/Downloads/quote.zip", "-d", "/home/richard/soal-shift-sisop-modul-3-ita06-2022/soal1/quote", NULL};

    if (num == 0)
    {
        processing(unzipping,music);
    }
    else{
        processing(unzipping,quote);
    }
}

//decoding base64
static char encoding_table[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H','I', 'J', 'K', 'L', 
                                'M', 'N', 'O', 'P','Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
                                'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f','g', 'h', 'i', 'j', 
                                'k', 'l', 'm', 'n','o', 'p', 'q', 'r', 's', 't', 'u', 'v',
                                'w', 'x', 'y', 'z', '0', '1', '2', '3','4', '5', '6', '7', 
                                '8', '9', '+', '/'};
static char *decoding_table = NULL;
static int mod_table[] = {0, 2, 1};

void build_decoding_table()
{

    decoding_table = malloc(256);

    for (int i = 0; i < 64; i++)
        decoding_table[(unsigned char)encoding_table[i]] = i;
}

void base64_cleanup()
{
    free(decoding_table);
}

char *base64_encode(const unsigned char *data,size_t input_length,size_t *output_length)
{

    *output_length = 4 * ((input_length + 2) / 3);

    char *encoded_data = malloc(*output_length);
    if (encoded_data == NULL)
        return NULL;

    for (int i = 0, j = 0; i < input_length;)
    {

        uint32_t octet_a = i < input_length ? (unsigned char)data[i++] : 0;
        uint32_t octet_b = i < input_length ? (unsigned char)data[i++] : 0;
        uint32_t octet_c = i < input_length ? (unsigned char)data[i++] : 0;

        uint32_t triple = (octet_a << 0x10) + (octet_b << 0x08) + octet_c;

        encoded_data[j++] = encoding_table[(triple >> 3 * 6) & 0x3F];
        encoded_data[j++] = encoding_table[(triple >> 2 * 6) & 0x3F];
        encoded_data[j++] = encoding_table[(triple >> 1 * 6) & 0x3F];
        encoded_data[j++] = encoding_table[(triple >> 0 * 6) & 0x3F];
    }

    for (int i = 0; i < mod_table[input_length % 3]; i++)
        encoded_data[*output_length - 1 - i] = '=';

    return encoded_data;
}

unsigned char *base64_decode(const char *data,size_t input_length,size_t *output_length)
{

    if (decoding_table == NULL)
        build_decoding_table();

    if (input_length % 4 != 0)
        return NULL;

    *output_length = input_length / 4 * 3;
    if (data[input_length - 1] == '=')
        (*output_length)--;
    if (data[input_length - 2] == '=')
        (*output_length)--;

    unsigned char *decoded_data = malloc(*output_length);
    if (decoded_data == NULL)
        return NULL;

    for (int i = 0, j = 0; i < input_length;)
    {

        uint32_t sextet_a = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        uint32_t sextet_b = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        uint32_t sextet_c = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        uint32_t sextet_d = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];

        uint32_t triple = (sextet_a << 3 * 6) + (sextet_b << 2 * 6) + (sextet_c << 1 * 6) + (sextet_d << 0 * 6);

        if (j < *output_length)
            decoded_data[j++] = (triple >> 2 * 8) & 0xFF;
        if (j < *output_length)
            decoded_data[j++] = (triple >> 1 * 8) & 0xFF;
        if (j < *output_length)
            decoded_data[j++] = (triple >> 0 * 8) & 0xFF;
    }

    return decoded_data;
}

void *decode(void *arg)
{
    int *n = (int *)arg;
    int num = *n;

    if (num == 0)
    {
        FILE *mout = fopen("music.txt", "w");
        for (int i = 1; i < 10; i++)
        {
            char c[100];
            FILE *fptr;
            char path[20] = "./music/m";
            char temp[20];
            sprintf(temp, "%d.txt", i);
            strcat(path, temp);
            fptr = fopen(path, "r");
            fscanf(fptr, "%[^\n]", c);
            long decode_size = strlen(c);
            char *plain = base64_decode(c, decode_size, &decode_size);
            fprintf(mout, "%s\n", plain);
            fclose(fptr);
        }
        fclose(mout);
    }
    else
    {
        FILE *qout = fopen("quote.txt", "w");
        for (int i = 1; i < 10; i++)
        {
            char c[100];
            FILE *fptr;
            char path[20] = "./quote/q";
            char temp[20];
            sprintf(temp, "%d.txt", i);
            strcat(path, temp);
            fptr = fopen(path, "r");
            fscanf(fptr, "%[^\n]", c);
            long decode_size = strlen(c);
            char *plain = base64_decode(c, decode_size, &decode_size);
            fprintf(qout, "%s\n", plain);
            fclose(fptr);
        }
        fclose(qout);
    }
}

void zip()
{
    char *zip_file[] = {"zip", "-r", "-m", "-q", "hasil.zip", "-P", "mihinomenestrichard", "hasil", NULL};
    processing(zipping,zip_file);
}

void move_file()
{
    char* file_music[] = {"mv", "music.txt", "./hasil", NULL};
    char* file_quote[] = {"mv", "quote.txt", "./hasil", NULL};
    processing(moving,file_music);
    processing(moving,file_quote);
}

void add_file_text()
{
    char *unzip_hasil[] = {"unzip", "-P", "mihinomenestrichard", "-qo", "hasil.zip", "-d", "/home/richard/soal-shift-sisop-modul-3-ita06-2022/soal1", NULL};
    processing(unzipping,unzip_hasil);
    FILE *add_file = fopen("no.txt", "w");
    fprintf(add_file, "No");
    fclose(add_file);
    char* file_no[] = {"mv", "no.txt", "./hasil", NULL};
    processing(moving,file_no);
}


int main(){
    char* music[]={"mkdir","music",NULL};
    char* quote[]={"mkdir","quote",NULL};
    processing(make_dir,music);
    processing(make_dir,quote);
    int m[]={0,1};
    pthread_t t_id[2];
    for (int i = 0; i < 2; i++)
    {
        pthread_create(&(t_id[i]), NULL, &unzip, (void *)&m[i]);
    }
    for (int i = 0; i < 2; i++)
    {
        pthread_join(t_id[i], NULL);
    }

    int n[]={0,1};
    pthread_t t_idd[2];
    for (int i = 0; i < 2; i++)
    {
        pthread_create(&(t_idd[i]), NULL, &decode, (void *)&n[i]);
    }
    for (int i = 0; i < 2; i++)
    {
        pthread_join(t_idd[i], NULL);
    }

    char* hasil[]={"mkdir","hasil",NULL};
    processing(make_dir,hasil);
    move_file();

    zip();

    add_file_text();
    zip();
    return 0;
}
