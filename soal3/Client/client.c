#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <sys/stat.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#define PORT 8080

void send_file(FILE *file, int sockfd);
void zip(char *zipping);
  
int main(int argc, char const *argv[]) 
{
    struct sockaddr_in address;
    int sock = 0, valread;
    struct sockaddr_in serv_addr;


    char buffer[1024] = {0};
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("\n Socket creation error \n");
        return -1;
    }
  
    memset(&serv_addr, '0', sizeof(serv_addr));
  
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);
      
    if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0) {
        printf("\nInvalid address/ Address not supported \n");
        return -1;
    }
  
    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        printf("\nConnection Failed \n");
        return -1;
    }

    zip("zip -o -r /home/alda/soal-shift-sisop-modul-3-ita06-2022/soal3/Client/hartakarun.zip ../hartakarun");


    send(sock, "hartakarun.zip", BUFSIZ, 0);

    return 0;
}

void zip(char *ziping)
{
    FILE *file;
    char buffer[BUFSIZ+1];
    int chars_read;

    memset(buffer, '\0', sizeof(buffer));

    //ziping
    file = popen(ziping, "w");

    if(file != NULL){
        chars_read = fread(buffer, sizeof(char), BUFSIZ, file);
        if(chars_read > 0) printf("%s\n", buffer);
        pclose(file);
    }
}
